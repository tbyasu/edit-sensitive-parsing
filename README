# Edit sensitive parsing (ESP)

Overview

## Description
ESP is a software for building integer vectors from input strings.
Edit distance with moves (EDM) is a string distance measure consisting of edit operations such as insertion, deletion and replacement in addition to substring move. 
ESP can convert input strings in the EDM space into integer vectors in the L1 distance space. 
These integer vectors are useful for similarity searches and classifications for strings, etc. 

## Quick Start
cd src
make
./esp --input_file=../dat/dat.txt  --output_file=vec.txt

## Usage
usage: ./esp --input_file=string --output_file=string [options] ... 
options:
  -i, --input_file     input file name (string)
  -o, --output_file    output file name (string)
  -h, --max_height     maxmum height of parse trees (unsigned long long [=18446744073709551615])
  -a, --load_factor    load factor (double [=0.5])
  -t, --tfidf          compute TF-IDF (bool [=0])
  -?, --help           print this message

## License
GPL version3

## Author
[Yasuo Tabei](https://sites.google.com/site/yasuotabei/)
