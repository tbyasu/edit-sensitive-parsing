/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>

#include "ESP.hpp"
#include "cmdline.h"

using namespace std;

int main(int argc, char **argv) {
  cmdline::parser p;
  p.add<string>("input_file",  'i', "input file name",  true);
  p.add<string>("output_file", 'o', "output file name", true);
  p.add<uint64_t>("max_height", 'h', "maxmum height of parse trees", false, 0xffffffffffffffff);
  p.add<double>("load_factor", 'a', "load factor", false, 0.5);
  p.add<bool>("tfidf", 't', "compute TF-IDF", false, false);
  
  p.parse_check(argc, argv);
  const string   input_file  = p.get<string>("input_file");
  const string   output_file = p.get<string>("output_file");
  const uint64_t max_height  = p.get<uint64_t>("max_height");
  const double   load_factor = p.get<double>("load_factor");
  const bool     tfidf       = p.get<bool>("tfidf");

  ESP e;
  e.run(input_file.c_str(), output_file.c_str(), max_height, load_factor, tfidf);
  
  return 0;
}
